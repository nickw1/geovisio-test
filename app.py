import os
import dotenv
import geovisio
from flask import Flask

dotenv.load_dotenv()
print(os.environ.get("FS_URL"))
app = Flask(__name__, template_folder="templates")
app = geovisio.create_app(app=app)
app.run()
