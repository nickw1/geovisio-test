# GeoVisio Test App (likely become the new opentrailview)

## Installation

I have prepared a draft PyPI package on testpypi containing the GeoVisio API -see [here](https://gitlab.com/geovisio/api/-/merge_requests/78) 

The package is listed in `requirements.txt`. To install, please use the following; this is because `geovisio` is in the TestPyPI repository, but the dependencies are in the main PyPI repository.
```
pip install --index-url https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple -r requirements.txt
```

Note that this uses the GeoVisio web viewer as well as the server. I have encountered problems with the hosted versions, possibly related to [this](https://gitlab.com/geovisio/web-viewer/-/issues/4), so I built from source. I have included my own built versions of the web viewer here, see [here](https://gitlab.com.geovisio/web-viewer) for original source.
